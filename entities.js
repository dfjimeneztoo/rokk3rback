/**
 * relations
 * 1-1
 * {
        type: "1-1",
        name: "category",
        foreign_name: "name",
        entity: "category",
        leftKey: "id_category"
      }
 * 1-n rightKey 
 * {
        type:"1-n",
        name:"history",
        rightKey:"id_task",
        entity:"task_status"
  }
 * n-n intermediate table
 * {
      type: "n-n",
      entity: "role",
      name: "roles",
      intermediate: {
        entity: "user_role",
        leftKey: "id_user",
        rightKey: "id_role"
      }
    }
 */
if (!dmt) {
  var dmt = {}
}
dmt.entities = {
  
}
module.exports = dmt;