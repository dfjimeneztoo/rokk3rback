'use strict'
var Backend = function (configJSON) {
  var config = configJSON || require('./config.json')
  var Generator = require('./app/generator/mysql-parser.js')
  var generator = new Generator()
  generator.parse(true).then(()=>{
    require('./app/events/events.js')()
  })
  var verbose = config.verbose === true

  // If we are using Google app engine to deploy the app
  if (config.googleDebug === true) {
    require('@google/cloud-trace').start()
    require('@google/cloud-debug')
  }

  // We create an Express app and enable Cross-origin resource sharing (CORS)
  var express = require('express')
  var methodOverride = require('method-override')
  var cors = require('cors')
  var path = require('path')

  var app = express()
  if (verbose) {
    var morgan = require('morgan')
    app.use(morgan('dev')) // Log every request to the console
  }
  app.use(express.static(path.join(__dirname, 'public')))
  app.use(methodOverride('X-HTTP-Method-Override')) // Override with the X-HTTP-Method-Override header in the request
  var whitelist = config.autorizedHosts
  var corsOptions = {
    origin: function (origin, callback) {
      var originIsWhitelisted = whitelist.indexOf(origin) !== -1
      callback(null, originIsWhitelisted)
    },
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE'
  }

  // Sign with default (HMAC SHA256)
  app.use(cors(corsOptions))

  // Enabling CORS Pre-Flight, for DELETE
  app.options('*', cors())

  // The routing logic of the app will be on this file.
  require('./app/routes.js')(app)

  // This turns on the app
  app.set('port', (process.env.PORT || config.port || 5000))

  var server

  this.start = function () {
    return new Promise((resolve, reject) => {
      server = app.listen(app.get('port'), () => {
        if (verbose) {
          console.log('Node app is running on port', app.get('port'))
          console.log('Using profile: ' + config.enviroment)
        }
        resolve(true)
      })
    })
  }

  this.close = function () {
    server.close()
  }
}

module.exports = Backend

if (module === require.main) {
  var instance = new Backend()
  instance.start()
}
