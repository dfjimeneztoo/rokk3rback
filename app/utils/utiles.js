var Config = require('../../config.json')
var Errores = require('./errors.js')
var Permissions = require('./permissions.js')
var Jwt = require('jsonwebtoken')
var Crypto = require('crypto')
var Nodemailer = require('nodemailer')

module.exports = {
  // This one is used in development.
  unimplementedMethod: function () {
    return { info: 'Unimplemented Method' }
  },
  //
  informError: function (num) {
    return { error: Errores[num] }
  },
  // This one is used to validate input arguments.
  isPositiveInteger: function (num) {
    if (!num) return false
    num = parseInt(num)
    return num === 0 || ((num | 0) > 0 && num % 1 === 0)
  },
  // This one is used to validate input arguments.
  isArrayOfPositiveIntegers: function (array) {
    if (!Array.isArray(array) || array.length === 0) return false
    for (var i = 0; i < array.length; i++) {
      if (!this.isPositiveInteger(array[i])) return false
    }
    return true
  },
  // This one is used to validate input arguments.
  isTimeStamp: function (timestamp) {
    return (new Date(timestamp)).getTime() > 0
  },
  // This one is used to clean the data before a SQL query.
  filterSqlInjection: function (txt) {
    // TODO implement method
    return txt
  },
  // This one is used in the authorization controller.
  decode: function (token) {
    return Jwt.decode(token, Config.secret)
  },
  // This one is used in the authorization controller.
  sign: function (user) {    
    return Jwt.sign(user, Config.secret)
  },
  // This one is used in the authorization controller.
  createHmac: function (algorithm) {
    return Crypto.createHmac(algorithm, Config.secret)
  },
  authorize: function (token, permit) {
    return new Promise((resolve, reject) => {
      return resolve(true);
    })
  },
  sendEmail: function (to, cc, bcc, subject, body, attachment) {
    return new Promise((resolve, reject) => {
      if(!to){
        reject("no receipt")
      }
      var transporter = Nodemailer.createTransport(Config.smtp.protocol + '://' + Config.smtp.sender + ':' + Config.smtp.password + '@' + Config.smtp.server)
      var mailOptions = {
        from: Config.smtp.from, // sender address
        to: to, // list of receivers
        cc: cc,
        bcc: bcc,
        subject: subject, // Subject line
        html: body // plaintext body
      }
      //send Mail
      // send mail with defined transport object
      transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
          reject(error)
        }
        resolve()
      })
    })
  },
}
