/*
This are the reported errors.
*/
module.exports = {
    TASK:{
        name: 'required|string',
        priority: 'required|numeric|min:1|max:5',
        dueDate: 'required|date'
    }
  }
  