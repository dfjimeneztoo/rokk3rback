if(!dmt){
	var dmt = {}
}
dmt.tables = {
	"task": {
		"fields": [
			{
				"name": "id",
				"type": "int",
				"disabled": true,
				"key": true
			},
			{
				"name": "name",
				"type": "text",
				"disabled": false,
				"key": false
			},
			{
				"name": "priority",
				"type": "int",
				"disabled": false,
				"key": false
			},
			{
				"name": "dueDate",
				"type": "date",
				"disabled": false,
				"key": false
			},
			{
				"name": "createdAt",
				"type": "datetime",
				"disabled": true,
				"key": false
			},
			{
				"name": "updatedAt",
				"type": "datetime",
				"disabled": true,
				"key": false
			}
		],
		"defaultSort": "id"
	}
};
try {
	module.exports = dmt;
} catch (e) {
	console.log(e);
}
