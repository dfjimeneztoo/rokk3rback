if (!dmt) {
	var dmt = {}
}
dmt.api= {
    version:"1.0",
    endpoints:[
        {
            controller:"task",
            entities:[
                {
                    entity:"task",
                    permissions:{
                        read:"none",
                        write:"none",
                        update:"none",
                        delete:"none"
                    }
                }
            ]
        }
    ]
}
module.exports = dmt;
